import time
from naoqi import ALProxy
import argparse
import threading

def standing_posture(motion, posture):
    posture.goToPosture("Stand", 1)

def idle_posture(motion, posture, speed):
    posture.goToPosture("Crouch", speed)
    motion.setStiffnesses("Body", 0.1)

def bad_posture(motion, posture):
    posture.goToPosture("Crouch", 1.0)
    joint_names = ["LHipYawPitch", "RHipYawPitch"]

    joint_angles = [-0.9, -0.9]

    motion.setAngles(joint_names, joint_angles, 0.05)
    motion.waitUntilMoveIsFinished()
    

def disable_autonomous_behaviors(args):
    autonomous_life = ALProxy("ALAutonomousLife", args.ip, args.port)
    basic_awareness = ALProxy("ALBasicAwareness", args.ip, args.port)
    autonomous_life.setState("disabled")
    basic_awareness.stopAwareness()

def stretch(motion, posture):
    standing_posture(motion, posture)
    
    yaw_angles = [-1.5, 1.5, 0]
    speed = 0.1

    for angle in yaw_angles:
        motion.setAngles("HeadYaw", angle, speed)
        time.sleep(5)

def set_all_leds_color(leds, color, stop_event):
    led_thread = threading.Thread(target=breathing_led_effect, args=(leds, color, stop_event))
    led_thread.start()
    return led_thread

def breathing_led_effect(leds, color, stop_event):
    colors = {
        "red": (1.0, 0.0, 0.0),
        "green": (0.0, 1.0, 0.0),
        "blue": (0.0, 0.0, 1.0),
        "white": (1.0, 1.0, 1.0),
        "off": (0.0, 0.0, 0.0)
    }
    rgb = colors.get(color.lower(), (0.0, 0.0, 0.0))
    led_groups = ["FaceLeds", "ChestLeds", "FeetLeds", "EarLeds"]

    while True:
        for intensity in list(range(0, 20, 2)) + list(range(30, -1, -2)):
            scaled_rgb = tuple([c * (intensity / 100.0) for c in rgb])
            for group in led_groups:
                leds.fadeRGB(group, scaled_rgb[0], scaled_rgb[1], scaled_rgb[2], 0.01)
            
            if stop_event.is_set():
                return
            time.sleep(0.000001)

def main(args):
    motion = ALProxy("ALMotion", args.ip, args.port)
    posture = ALProxy("ALRobotPosture", args.ip, args.port)
    tts = ALProxy("ALTextToSpeech", args.ip, args.port)
    leds = ALProxy("ALLeds", args.ip, args.port)
    
    if args.leds:
        stop_event = threading.Event()
        led_thread = set_all_leds_color(leds, args.leds, stop_event)

    if args.text:
        tts.say(args.text)

    if args.action:
        return_speed = 1
        if args.action == "setup":
            tts.say("Setting up!")
            disable_autonomous_behaviors(args)
            idle_posture(motion, posture, return_speed)

        if args.action == 'idle':
            idle_posture(motion, posture, return_speed)
    
        if args.action == 'bad_posture':
            bad_posture(motion, posture)
            return_speed = 0.1

        if args.action == 'standing_posture':
            standing_posture(motion, posture)
        
        if args.action == 'neck_stretch':
            stretch(motion, posture)

        if args.duration:
            time.sleep(args.duration)
        idle_posture(motion, posture, return_speed)

    if args.leds:
        stop_event.set()    
        led_thread.join()
        set_all_leds_color(leds, "off", stop_event)

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='NAO Robot Controller')

    parser.add_argument('ip', type=str, help='The IP address of the NAO robot')
    parser.add_argument('port', type=int, help='The port number to connect to')
    parser.add_argument('--action', type=str, help='The action to perform', choices=['idle', 'bad_posture', 'standing_posture', 'neck_stretch', 'setup'])
    parser.add_argument('--leds', type=str, choices=['green', 'red', 'blue', 'white', 'off'], help='The lights setting for the NAO robot')
    parser.add_argument('--text', type=str, help='The text for the NAO robot')
    parser.add_argument('--duration', type=float, help='The duration to hold the posture for')

    args = parser.parse_args()

 
    main(args)

