FROM python:2.7.18-buster

RUN mkdir -p /app
WORKDIR /app

COPY sdk.tar.gz ./sdk.tar.gz
RUN tar -xvzf ./sdk.tar.gz && rm ./sdk.tar.gz

COPY * .

ENV PYTHONPATH "/app/pynaoqi-python2.7-2.8.7.4-linux64-20210819_141148/lib/python2.7/site-packages"


ENTRYPOINT ["python2", "nao.py"]
