NAOqi is linked to the script via environment variables!
You can do this manually or run the following command:
source pythonpath

You need to install python2.7. On debian based systems, you can do
sudo apt-get install python2.7

To run the script, you can look at the help command by running:
python2.7 nao.py -h

Here's an example command to help you along:
python2.7 nao.py 192.168.1.231 9559 --text "hello" --action="standing_posture" --leds=white --duration=5
